#!/usr/bin/env python
# coding: utf-8

# In[2]:


print('python')


# In[10]:


print("Syed," + "Manjinder,"+ "Banni_boy "+ "live in Neelkamal Apartment")


# In[24]:


comp_name="Hexaware Technologies"
print(len(comp_name))
#print (comp_name[0:7])
#print(comp_name[0:9:2])
#print(comp_name[::-1])


# In[25]:


billAmount=300000
taxAmount=25/100*billAmount
totalAmount=taxAmount+billAmount

print(totalAmount)


# In[32]:


fruits=["apple","mango","banana"]
print(fruits)


# In[33]:


fruits.reverse()


# In[34]:


print(fruits)


# In[37]:


fruits.append('Orange')


# In[38]:


print(fruits)


# In[39]:


fruits.insert(2,"grapes")


# In[40]:


print(fruits)


# In[45]:


fruits.pop(1)


# In[46]:


print(fruits)


# In[47]:


fruits.sort()


# In[48]:


print(fruits)


# In[50]:


fruits.remove("Orange")


# In[51]:


print(fruits)


# In[54]:


#print(fruits.split())


# 
# SETS
# 

# In[57]:


players={'Dhoni',"Virat",'Yuvraj','Manjinder','Manjinder'}
print(len(players))


# In[64]:


players.add('Yuvi')
players


# In[63]:


players


# TUPLES

# In[66]:


programming_languages=('Java','Java','Python','GoLang','Javascript')
print("Value at first index is:", programming_languages[0]);
print("Count of Java:",programming_languages.count('Java'))
print("Total elements:",len(programming_languages))


# In[70]:


for language in programming_languages:
    print("Programming Language is:"+ language)


# In[81]:


def operators(a=90,b=10):
    return (a+b),(a-b),(a*b),(a/b)


# In[84]:


operators(20)


# LAMBDA EXPRESSION
# 

# In[ ]:


children=map(lambda child:str(child)+"is less than 18", list(filter(lambda age:age<18)))


# In[86]:


def greet(name):
    return lambda message:message+name

vinodGreet=greet('Vinod')
vinodGreet=greet('Kiran')

print(vinodGreet("Hello "))


# In[ ]:




